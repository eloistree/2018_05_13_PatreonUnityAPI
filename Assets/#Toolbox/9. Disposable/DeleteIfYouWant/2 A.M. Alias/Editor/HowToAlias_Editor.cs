﻿
using UnityEditor;
using UnityEngine;

namespace J2AM.Toolbox.EditorScripts
{
    public class HowToAlias_Editor : Editor
    {

        [MenuItem("2 A.M./How To/Connect To/Patreon API")]
        public static void HT_Connect_Patreon() { Application.OpenURL("https://github.com/JamsCenter/2017_07_13_PatreonUnityAPI"); }
        [MenuItem("2 A.M./How To/Connect To/Facebook API")]
        public static void HT_Connect_Facebook() { Application.OpenURL("https://github.com/JamsCenter/2017_03_24_HowTo_DoReactionVideo/wiki/Draft:-2017-March,-24th-Friday"); }
        [MenuItem("2 A.M./How To/Access To/Social Network Avatars")]
        public static void HT_Access_Avatar() { Application.OpenURL("https://github.com/JamsCenter/2017_07_13_PatreonUnityAPI/wiki/Access-Social-Profiles"); }
        [MenuItem("2 A.M./How To/Start with/Virtual Reality")]
        public static void HT_StartWith_VirturalRality() { Application.OpenURL("https://github.com/JamsCenter/2017_06_15_HelloVirtualReality"); }
        [MenuItem("2 A.M./How To/Start with/Augmented Reality")]
        public static void HT_StartWith_AugmentedReality() { Application.OpenURL("https://github.com/JamsCenter/2017_06_15_HelloVirtualReality/wiki/Module-%2314:-What-is-augmented-and-mixed-reality"); }
        [MenuItem("2 A.M./How To/Start with/Reaction Video")]
        public static void HT_StartWith_ReactVideo() { Application.OpenURL("https://github.com/JamsCenter/2017_03_24_HowTo_DoReactionVideo"); }
        [MenuItem("2 A.M./How To/Start with/360")]                          //https://github.com/JamsCenter/2017_04_11_HowToUse360Camera/wiki
        public static void HT_StartWith_360() { Application.OpenURL("https://github.com/JamsCenter/2017_04_11_HowToUse360Camera/wiki"); }
        [MenuItem("2 A.M./How To/Start with/Gear VR")]
        public static void HT_StartWith_GearVR() { Application.OpenURL("https://github.com/JamsCenter/2017_06_15_HelloVirtualReality/wiki/Module-%2304:-Hello-Gear-VR-with-Unity3D"); }
        [MenuItem("2 A.M./How To/Start with/Cardboard")]
        public static void HT_StartWith_Cardboard() { Application.OpenURL("https://github.com/JamsCenter/2017_06_15_HelloVirtualReality/wiki/Module-%2307:-Hello-Cardboard-with-Unity3D"); }
        //[MenuItem("2 A.M./How To/Start with/Kinect")]
        //public static void HT_StartWith_() { Application.OpenURL("    "); }
        [MenuItem("2 A.M./How To/Start with/NFC")]                          //
        public static void HT_StartWith_NFC() { Application.OpenURL("https://github.com/JamsCenter/2017_06_28_HelloNFC"); }
        [MenuItem("2 A.M./How To/Start with/Arduino")]                      //
        public static void HT_StartWith_Arduino() { Application.OpenURL("https://github.com/JamsCenter/2017_04_26_HowToLearnIoT"); }
        [MenuItem("2 A.M./How To/Start with/Raspberry Pi")]                 //
        public static void HT_StartWith_Raspberry() { Application.OpenURL("https://github.com/JamsCenter/2017_04_26_HowToLearnIoT"); }
        [MenuItem("2 A.M./How To/Start with/Twitch Plays")]
        public static void HT_StartWith_TwitchPlay() { Application.OpenURL("https://github.com/JamsCenter/2016_08_28_TwitchPlays_Risk"); }


        ////public static void HT_ _()Application.OpenURL("http://jams.center/2AM/");}


    }
}