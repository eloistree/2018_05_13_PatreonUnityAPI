﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEditor;
using UnityEngine;

namespace J2AM.Toolbox.EditorScripts
{
    [InitializeOnLoad]
    public class Startup
    {
        static Startup()
        {
            PatreonReminder_Editor.LoadSettings();
            if (!string.IsNullOrEmpty(PatreonReminder_Editor.patreonMail))
                return;
            if (!string.IsNullOrEmpty(PatreonReminder_Editor.licenseKey))
                return;
            PatreonReminder_Editor.Init();
        }
    }

    public class PatreonReminder_Editor : EditorWindow
    {
         
        public static string patreonMail  = "";
        public static string licenseKey = "";
        public static string message = "";
        public static Texture2D patreonImage;
        public static string patreonCreditsLink ;
        public static string localStorage;


        [MenuItem("2 A.M./License")]
        public  static void Init()
        {
            // Get existing open window or if none, make a new one:
            PatreonReminder_Editor window = (PatreonReminder_Editor) GetWindowWithRect(typeof(PatreonReminder_Editor), new Rect(0, 0, 800, 650), true, "2 A.M. License");
            window.Show();
        }

        private  void LoadOnlineCreditsFromWeb(string filePath)
        {
            //string localFilename = @"c:\localpath\tofile.jpg";
            using (WebClient client = new WebClient())
            {
                client.DownloadFile(patreonCreditsLink, filePath);
            }
        }


        public  Texture2D LoadPNG(string filePath)
        {

            Texture2D tex = null;
            byte[] fileData;

            if (File.Exists(filePath))
            {
                fileData = File.ReadAllBytes(filePath);
                tex = new Texture2D(2, 2);
                tex.LoadImage(fileData); 
            }
            return tex;
        }

        private void Awake()
        {
            localStorage = Application.temporaryCachePath + "/PatreonCredit.png";
            patreonCreditsLink = "http://jams.center/2AM/Credits.png";
            LoadSettings();
            LoadOnlineCreditsFromWeb(localStorage);
            patreonImage = LoadPNG(localStorage);
            

        }
        void OnDestroy()
        {
            SaveSettings();
        }

        void OnGUI()
        {
            patreonMail = EditorGUILayout.TextField("Patreon Mail:", patreonMail);
            licenseKey = EditorGUILayout.TextField("License key:", licenseKey);

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Generate Key"))
            {
                if(string.IsNullOrEmpty(patreonMail))
                    licenseKey = "Please enter your email to confirm you know you are using a patreon licence.";
                else// if (string.IsNullOrEmpty(licenseKey))
                    licenseKey = "There is no license key needed you silly ;) !";
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("2 A.M."))
            {
                Application.OpenURL("http://jams.center/2AM/");

            }
            if (GUILayout.Button("Jams.Center"))
            {
                Application.OpenURL("http://jams.center/");

            }
            if (GUILayout.Button("About the license"))
            {
                Application.OpenURL("http://jams.center/2AM/License");

            }

            EditorGUILayout.EndHorizontal();
            if (patreonImage != null)
                EditorGUI.DrawPreviewTexture(new Rect(0, 200, 800, 450), patreonImage);


            //if ( ! string.IsNullOrEmpty(message))
            //    EditorGUILayout.HelpBox( message, MessageType.Info);

            EditorGUILayout.HelpBox("Please consider to be patreon if my code was helpful to you.\n  \nFeel free to contact me:\n   - Éloi Strée\n   - jams.center@gmail.com\n   - http://patreon.com/2AM/ \n  \n May the code be with you  \n             \\m/(-_-)\\m/    ", MessageType.Warning);

            //GUILayout.Label(m_Logo);

        }

        public static void SaveSettings()
        {
            if ( ! string.IsNullOrEmpty(patreonMail))
                EditorPrefs.SetString("PatreonMail", patreonMail);
            if ( ! string.IsNullOrEmpty(licenseKey))
                EditorPrefs.SetString("PatreonLicense", licenseKey);
         }
        public static void  LoadSettings()
        {
            patreonMail = EditorPrefs.GetString("PatreonMail", "");
            licenseKey = EditorPrefs.GetString("PatreonLicense", "");
        }


    }
}