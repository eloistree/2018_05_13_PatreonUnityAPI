﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Demo_DownloadPatreonUsers : MonoBehaviour {

    public string _recordPath;
    public int _index= 0;
    public float _durationBetweenCheck=0.1f;
    public InputField _indexInput;

    public int _max = 100000;
    public void Awake()
    {
        _index = PlayerPrefs.GetInt("Index");
        if (_indexInput)
            _indexInput.onEndEdit.AddListener(SetIndexTo);
    }
    public void OnDestroy()
    {
        PlayerPrefs.SetInt("Index",_index);
    }

    public void SetIndexTo(string index) {
        try {
            _index = int.Parse(index);

        }
        catch (Exception ) { }
    }
    void Start () {

        InvokeRepeating("DownloadNextUser", 0f, _durationBetweenCheck);
        Directory.CreateDirectory(_recordPath);
		
	}
    void DownloadNextUser() {

        PatreonAPI.Load(CreateFileFromUser, ""+_index, DoNothing);
        PatreonAPI.Load(CreateFileFromCampaign,""+ _index, DoNothing);
        _index++;
        _max--;
        _indexInput.text = ""+_index;
        if (_max < 0)
            Destroy(this);


    }

    private void DoNothing(string errorMessage)
    {
    }

    private void CreateFileFromCampaign(CampaignBean loaded)
    {
        string globalPath = _recordPath + "/_Campaign.csv";
        string line = loaded._id + ";" + loaded._creatorId + ";" + loaded._patreonCount + ";" + loaded._pledgeSum + "\r\n";
        File.WriteAllText(_recordPath + "/Compaign_" + loaded._id + ".json", JsonUtility.ToJson(loaded));
        File.AppendAllText(globalPath, line);
        
    }

    private void CreateFileFromUser(UserBean loaded)
    {
        string globalPath = _recordPath + "/_Users.csv";
        string line = loaded._id+";"+ loaded._userCampaignId + ";" + loaded._fullName + "\r\n";

        File.WriteAllText(_recordPath + "/Client_" + loaded._id + ".json", JsonUtility.ToJson(loaded));
        File.AppendAllText(globalPath, line);

    }

    private void OnValidate()
    {
        if (_recordPath == "")
            _recordPath = Application.dataPath;
    }
}
