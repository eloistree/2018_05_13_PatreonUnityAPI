﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_LoadImages : MonoBehaviour {

    public string _urlToLoad;
    public Texture _textureLoaded;
    public GameObject _meshToApply;
    public UnityEngine.UI.RawImage _imageRawToApply;

    public void Start()
    {
        LoadAll();
    }

    public void LoadAll()
    {
        _textureLoaded = ImageGenerator.CreateRandomTexture(ImageGenerator.ImageSize.x256);
        ImageDownloader.Load(_urlToLoad, _meshToApply);
        ImageDownloader.Load(_urlToLoad, _imageRawToApply);
        ImageDownloader.Load(_urlToLoad, DisplayTexture);
    }

    private void DisplayError(string message)
    {
        Debug.Log("Error:" + message);
    }

    private void DisplayTexture(Texture texture)
    {
        _textureLoaded = texture;
        _meshToApply.GetComponent<Renderer>().material.mainTexture = texture;

    }
}
