﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo_PatreonUsersToGrid : MonoBehaviour {

    public GridLayoutGroup _gridToAffect;
    public GameObject _uiAvatarsPrefab;
    public PatreonList _patreonList;
    public PatreonIdentifiers.SortBy _sortBy = PatreonIdentifiers.SortBy.Random;
    public List<UserBean> _usersList;
    public ScreenCapture _screenCapture;
    public AutoResizeGrid _gridResizer;

	// Use this for initialization
	void Start () {
        LoadAllUsers();
    }

    public void LoadAllUsers() {
        List<string> ids = _patreonList.GetPatreon(_sortBy);
        for (int i = 0; i < ids.Count; i++)
        {
            if (!string.IsNullOrEmpty(ids[i]))
                AddUser(ids[i]);
        }

    }
    void AddUser(string id)
    {
        PatreonAPI.Load(AddUserLoaded, id, DestroyOnError);
    }

    private void DestroyOnError(string errorMessage)
    {
        Debug.LogWarning(errorMessage);
        Destroy(gameObject);
    }

    void AddUserLoaded(UserBean user) {
        List<string> avatarUrls = user.GetAvatarUrls();
        if (avatarUrls.Count <= 0)
            return;
        AddUser(user.GetId(), avatarUrls);
        _usersList.Add(user);
    }
    // Update is called once per frame
    void AddUser (string id, List<string> urls ) {

        GameObject created = Instantiate(_uiAvatarsPrefab);
        created.transform.SetParent(_gridToAffect.transform);
        UI_Avatars avatars = created.GetComponent<UI_Avatars>();
        if (avatars != null)
            avatars.SetImages(urls);


    }

    public IEnumerator TakeScreenShotWhileNextPage() {

        bool hasNextPage;
        do
        {
            _screenCapture.LaunchScreenShot();
            yield return new WaitForSeconds(0.1f);
            hasNextPage = _gridResizer.NextPage();
            Debug.Log("Hym ? " + hasNextPage);
            yield return new WaitForSeconds(0.1f);
        }
        while (hasNextPage);

    }
    public void TakeScreenshot() {
        StartCoroutine(TakeScreenShotWhileNextPage());
    }
}
