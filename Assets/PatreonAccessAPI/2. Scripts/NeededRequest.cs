﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface NeededRequest{



    List<string> GetNewPatreonsIdSinceLastCheck();

    List<string> GetCurrentPatreonsId();
    List<string> GetAlltimePatreonsId();

    float GetCampaignComplition();
    int GetPreviousDonation();
    int GetNextMonthDonation();
    /// ...
}

//public delegate void OnNewPatreon();