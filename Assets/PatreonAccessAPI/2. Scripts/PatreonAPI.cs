﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;
using System.Text;

[ExecuteInEditMode]
public class PatreonAPI : MonoBehaviour {

    #region SINGLETON ACCESS 
    private static PatreonAPI _instanceInScene;
    public static PatreonAPI Instance
    {
        get
        {   //////// Return Instance :) ///////
            bool hasInstance = _instanceInScene != null;
            if (hasInstance)
                return _instanceInScene;
            //////// Find the instance in scene /////
            _instanceInScene = FindObjectOfType<PatreonAPI>();

            hasInstance = _instanceInScene != null;
            if (hasInstance)
                return _instanceInScene;

            ///////// Create the instance in the scene////////////////
            GameObject createdLoader = new GameObject("# Patreon API");
            _instanceInScene = createdLoader.AddComponent<PatreonAPI>();

            return _instanceInScene;
        }
        private set
        {
           // if (_instanceInScene == null)
                _instanceInScene = value;
        }
    }

    public void Awake()
    {
        Instance = this;
        LoadKeysBackup();
       // DontDestroyOnLoad(this.gameObject);
    }

    private void OnDisable()
    {
        SaveKeysBackup();
    }
    public void OnApplicationQuit()
    {
        Instance = null;
        SaveKeysBackup();
    }
    #endregion

    #region INSPECTOR PARAMS

    [SerializeField]
    private AccessKeys _accessKeys = new AccessKeys();
    [SerializeField]
    private PatreonToken _patreonToken = new PatreonToken();
    [SerializeField]
    private PermissionCode _permissionCode = new PermissionCode();


    public AccessKeys Keys { get { return _accessKeys; } private set { _accessKeys = value; } }
    public PatreonToken Token { get { return _patreonToken; } private set { _patreonToken = value; } }
    private PermissionCode PermissionCode { get { return _permissionCode; } }

    [Header("URL")]
    [SerializeField]
    private string _apiUrlToken = "api.patreon.com/oauth2/token";
    [SerializeField]
    private string _redirectUrl = "http://";

    #endregion

    #region BACK UP METHODE

    public static bool IsTokenExpired()
    {
        return Instance.Token.TimeLeft <= 0;
    }
    public void LoadKeysBackup()
    {
        Token = JsonUtility.FromJson<PatreonToken>(PlayerPrefs.GetString("Token"));
        Keys = JsonUtility.FromJson<AccessKeys>(PlayerPrefs.GetString("AccessKeys"));
    }
    public static void Load()
    {
        Instance.Token = JsonUtility.FromJson<PatreonToken>(PlayerPrefs.GetString("Token"));
        Instance.Keys = JsonUtility.FromJson<AccessKeys>(PlayerPrefs.GetString("AccessKeys"));
      
    }
    public void SaveKeysBackup() {
        PlayerPrefs.SetString("Token", JsonUtility.ToJson(Token));
        PlayerPrefs.SetString("AccessKeys", JsonUtility.ToJson(Keys));
    }
    internal static void Save()
    {
        PlayerPrefs.SetString("Token", JsonUtility.ToJson(Instance.Token));
        PlayerPrefs.SetString("AccessKeys", JsonUtility.ToJson(Instance.Keys));

    }
    #endregion

    #region CONNECTION AND INITI METHODE

    public static bool HasPermissionCode()
    {
        return Instance.PermissionCode.HasCode();
    }

    public static bool HasClientId()
    {
        return !string.IsNullOrEmpty(Instance.Keys.ClientId);
    }

    public static bool HasClientSecret()
    {
        return !string.IsNullOrEmpty(Instance.Keys.ClientSecret);
    }

    public static void SetClientKeys(string clientId, string clientSecret)
    {
        Instance.Keys.SetKeys(clientId, clientSecret);
    }

    public static void SetPermissionCode(string code)
    {
        Instance.PermissionCode.SetCode(code);
    }
    public static bool HasToken()
    {
        return !string.IsNullOrEmpty(Instance.Token.Token);
    }

    public static void TryGenerateToken()
    {
        TryGenerateToken(GetPermissionCode(true));
    }

    public static void TryGenerateToken(string permissionCode)
    {
        PatreonBuilder.OnTokenGenerated parse = delegate (PatreonToken token)
        {
            Instance.Keys.SetToken(token.Token);
            Instance.Token = token;
        };
        PatreonBuilder.GetNewToken(parse, Instance._apiUrlToken, permissionCode, Instance.Keys.ClientId, Instance.Keys.ClientSecret, Instance._redirectUrl);
    }

    public static void RefreshToken()
    {
        PatreonBuilder.OnTokenGenerated parse = delegate (PatreonToken token)
        {
            Instance.Keys.SetToken(token.Token);
            Instance.Token = token;
        };
        PatreonBuilder.GetRefreshToken(parse, Instance._apiUrlToken, Instance.Token.Refresh, Instance.Keys.ClientId, Instance.Keys.ClientSecret);
    }


    internal static bool IsAlmostExpired()
    {
        return Instance.Token.TimeLeft < 2569200;//3 Jours
    }

    internal static string GetPermissionCode(bool useDestructifAccess)
    {
        return Instance.PermissionCode.GetCode(useDestructifAccess);
    }

    #endregion

    #region ACCESS DATA
    public static void DownloadAllUserInformation()
    {

    }


    public static void Download_PublicUserProfile(string userId) {

    }
    #endregion

    #region URL GENERATION AND CALL BROWSER

    public static void OpenBrowser_ForClientInformation()
    {
        Application.OpenURL("https://www.patreon.com/platform/documentation/clients");
    }
    public static void OpenBrowser_ForUserAuthorization()
    {
        string url = GenerateURL_AskForAuthorizationCode(Instance.Keys.ClientId, Instance._redirectUrl, null, null);
        Application.OpenURL(url);
    }

    public string GenerateURL_TokenAccess(string permissionCode, string clientId, string clientSecret, string redirection)
    {
        string askAuthorization =
            _apiUrlToken +
                "?code=" + permissionCode +
                "&grant_type=authorization_code" +
                "&client_id=" + clientId +
                "&client_secret=" + clientSecret +
                (string.IsNullOrEmpty(redirection) ?"": ("&redirect_uri=" + redirection) );
        return askAuthorization;
    }

    public static string GenerateURL_AskForAuthorizationCode(string clientId, string redirectionURL = null, string scope = null, string state = null)
    {
        string askAuthorization =
              "www.patreon.com/oauth2/authorize"
              + "?response_type=code&client_id={0}"
              + (redirectionURL != "http://" && redirectionURL.Length > 0 ? "&redirect_uri={1}" : "")
              + (string.IsNullOrEmpty(scope) ? "" : "&scope={2}") + (string.IsNullOrEmpty(state ) ? "" : "&state={3}");
        return string.Format(askAuthorization, clientId, redirectionURL, scope, state);
    }
    #endregion

    #region RESET
    public static void Reset(bool key, bool token)
    {
        if (key)
            Instance.Keys.Reset();
        if (token)
            Instance.Token.Reset();
        Save();
    }

    internal static void ResetToDefault()
    {
        Instance.Keys.Reset();
        Instance.Token.Reset();
        Instance.PermissionCode.Reset();
    }
    #endregion



    public static void Load(OnUserLoaded onUserloaded, PatreonBuilder.OnErrorBuilding onError = null)
    {
        PatreonBuilder.CreateMyInfo(Instance.Token.Token, onUserloaded, onError == null ? DisplayError : onError);
    }
    public static void Load(OnCompaignLoaded onCampaignLoaded, PatreonBuilder.OnErrorBuilding onError = null)
    {
        PatreonBuilder.CreateMyCampaign(Instance.Token.Token, onCampaignLoaded, onError == null ? DisplayError : onError);
    }


    public static void Load(OnUserLoaded onUserloaded, string userId, PatreonBuilder.OnErrorBuilding onError =null)
    {
        PatreonBuilder.Create(userId , onUserloaded, onError == null ? DisplayError : onError);
    }
    public static void Load(OnCompaignLoaded onCampaignLoaded, string campaingId,PatreonBuilder.OnErrorBuilding onError=null)
    {
        PatreonBuilder.Create(campaingId , onCampaignLoaded, onError==null? DisplayError:onError);
    }

    private static void DisplayError(string errorMessage)
    {
        Debug.LogWarning("Creation Error:" + errorMessage);
    }
}

public delegate void OnUserLoaded(UserBean loaded);
public delegate void OnCompaignLoaded(CampaignBean loaded);



