﻿using PatreonUnityAPI.Public.Campaign;
using PatreonUnityAPI.Public.User;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.XPath;
using UnityEngine;

public class PatreonBuilder  {


    #region PUBLIC WEB PAGE ACCESS



    #endregion

    #region API ACCESS


    #endregion

    #region TOKEN ACCESS
    public delegate void OnTokenGenerated(PatreonToken token);
    public static void GetNewToken(OnTokenGenerated onGenerated, string apiTokenUrl, string userCode, string clientId, string clientSecret, string urlRedirect )
    {
         PatreonJsonLoader.OnLoaded_Token parse = delegate (ref J_Token jValue)
        {

            PatreonToken mToken = JsonToPatreon(jValue);
            onGenerated(mToken);
        };
        PatreonJsonLoader.Instance.LoadToken(apiTokenUrl, userCode, clientId, clientSecret, urlRedirect,parse, DisplayError);
    }
    public static void GetRefreshToken(OnTokenGenerated onGenerated, string apiTokenUrl, string refreshToken, string clientId, string clientSecret)
    {
        PatreonJsonLoader.OnLoaded_Token parse = delegate (ref J_Token jValue)
        {
            PatreonToken mToken = JsonToPatreon(jValue);
            onGenerated(mToken);
        };
        PatreonJsonLoader.Instance.LoadNewToken(apiTokenUrl,  refreshToken,  clientId,  clientSecret, parse, DisplayError);
    }
    #region JSON TO BEAN
    private static PatreonToken JsonToPatreon(J_Token token)
    {
        return new PatreonToken(token.access_token,
            token.refresh_token,
            int.Parse(token.expires_in),
            token.scope,
            token.token_type);
    }

    #endregion
    #endregion

    #region CREATE FROM API 
    internal static void CreateMyInfo(string token,OnUserLoaded onUserloaded, OnErrorBuilding onError = null)
    {
         PatreonJsonLoader.OnLoaded_UserInfo onLoaded = delegate (ref JsonPublicData_User jUser) {

             UserBean user = CreateUserFromJsonInfo(jUser);
             onUserloaded(user);
         };
        PatreonJsonLoader.OnErrorLoading onLoadingError = delegate (string errorMessage) {
            if (onError != null)
                onError(errorMessage);
        };

        PatreonJsonLoader.Instance.LoadApi_UserInformation (token, onLoaded, onLoadingError);

    }
    internal static void CreateMyCampaign(string token,OnCompaignLoaded onUserloaded, OnErrorBuilding onError = null)
    {
        PatreonJsonLoader.OnLoaded_CampaignInfo onLoaded = delegate (ref JsonCompaignInfo jCampaign, ref List<JsonCompaignIncludedInfo> jIncludedInfo) {

            CampaignBean campaign = CreateCampaignFromJsonInfo(jCampaign, jIncludedInfo);
            onUserloaded(campaign);
        };
        PatreonJsonLoader.OnErrorLoading onLoadingError = delegate (string errorMessage) {
            if (onError != null)
                onError(errorMessage);
        };

        PatreonJsonLoader.Instance.LoadApi_CompaignInformation(token, onLoaded, onLoadingError);

    }
    internal static void CreateMyPledge(string token,OnUserLoaded onUserloaded, OnErrorBuilding onError = null)
    {
        //// TODO /////
    }
    #endregion



    #region CREATE FROM WEB
    internal static void Create(string userId, OnUserLoaded onUserloaded, OnErrorBuilding onError=null)
    {
        PatreonJsonLoader.OnLoaded_UserInfo onLoaded = delegate (ref JsonPublicData_User userInfo)
        {
            UserBean user = CreateUserFromJsonInfo(userInfo);
            onUserloaded(user);
        };
        PatreonJsonLoader.OnErrorLoading onLoadingError = delegate (string errorMessage) {
            if(onError!=null)
                onError(errorMessage);
        };

        PatreonJsonLoader.Instance.LoadWeb_UserInformation(userId, onLoaded, onLoadingError);
        

    }
    internal static void Create(string campaignId, OnCompaignLoaded onUserloaded, OnErrorBuilding onError = null)
    {
            PatreonJsonLoader.OnLoaded_CampaignInfo onLoaded = delegate (ref JsonCompaignInfo jCampaign, ref List<JsonCompaignIncludedInfo> jIncludedInfo) {

            CampaignBean campaign = CreateCampaignFromJsonInfo(jCampaign, jIncludedInfo);
            onUserloaded(campaign);
        };
        PatreonJsonLoader.OnErrorLoading onLoadingError = delegate (string errorMessage) {
            if (onError != null)
                onError(errorMessage);
        };

        PatreonJsonLoader.Instance.LoadWeb_CompaignInformation(campaignId, onLoaded, onLoadingError);
    }

    public delegate void OnErrorBuilding(string errorMessage);
    #endregion

    #region CONVERTER
    private static UserBean CreateUserFromJsonInfo(JsonPublicData_User userInfo)
    {
        UserBean user = new UserBean();
        user._id = userInfo.id;
        user._aliasName = userInfo.attributes.vanity;
        user._about = userInfo.attributes.about;
        user._firstName = userInfo.attributes.first_name;
        user._lastName = userInfo.attributes.last_name;
        user._fullName = userInfo.attributes.full_name;
        user._gender = userInfo.attributes.gender == 0 ? UserBean.Gender.Male : UserBean.Gender.Female;
        user._onPatreonSince = Timestamp.Create(userInfo.attributes.created);

       // user._userCampaignId = userInfo.relationships.campaign.data.id;
        user._social.Facebook.SetGivenData(
                userInfo.attributes.facebook,null);
        user._social.Twitch.SetGivenData(
                userInfo.attributes.twitch, null);
        user._social.Twitter.SetGivenData(
                userInfo.attributes.twitter, null);
        user._social.Youtube.SetGivenData(
                userInfo.attributes.youtube, null);

        user._email = userInfo.attributes.email;
        

        if (userInfo.relationships.campaign.data!=null)
        user._userCampaignId = userInfo.relationships.campaign.data.id;


        if (userInfo.attributes.social_connections!=null) {

            user._social.DeviantArt.SetGivenData(
                null,
                userInfo.attributes.social_connections.deviantart.user_id
                );

            user._social.Facebook.SetGivenData(
                null, userInfo.attributes.social_connections.facebook.user_id);
            user._social.Spotify.SetGivenData(
                null, userInfo.attributes.social_connections.spotify.user_id);
            user._social.Twitch.SetGivenData(
                null, userInfo.attributes.social_connections.twitch.user_id);
            user._social.Twitter.SetGivenData(
                null, userInfo.attributes.social_connections.twitter.user_id);
            user._social.Youtube.SetGivenData(
                null, userInfo.attributes.social_connections.youtube.user_id);
            user._social.Discord.SetGivenData(
                null, userInfo.attributes.social_connections.discord.user_id);
        }


        if (!string.IsNullOrEmpty(userInfo.attributes.email))
        {
            user._oAuthPlus._isNuked = userInfo.attributes.is_nuked;
            user._oAuthPlus._isEmailVerified = userInfo.attributes.is_email_verified;
            user._oAuthPlus._is_suspended = userInfo.attributes.is_suspended;
            user._oAuthPlus._is_deleted = userInfo.attributes.is_deleted;
            user._oAuthPlus._has_password = userInfo.attributes.has_password;
        }
        user._images.SetPatreonImageTo(userInfo.attributes.image_url , userInfo.attributes.thumb_url);


        return user;
    }
    public static CampaignBean CreateCampaignFromJsonInfo(JsonCompaignInfo jCampaign, List<JsonCompaignIncludedInfo> jIncludedInfo)
    {
        CampaignBean campaign = new CampaignBean();
        campaign._id = jCampaign.id;
        campaign._createdSince = Timestamp.Create(jCampaign.attributes.created_at);
        campaign._publishedSince = Timestamp.Create(jCampaign.attributes.published_at);
        campaign._summaryMessage = jCampaign.attributes.summary;
        campaign._thanksMessage = jCampaign.attributes.thanks_msg;
        campaign._creationName = jCampaign.attributes.creation_name;
        campaign._creationOneLiner = jCampaign.attributes.one_liner;
        campaign._patreonCount = jCampaign.attributes.patron_count;
        campaign._pledgeSum = jCampaign.attributes.pledge_sum;
        campaign._pledgeType = jCampaign.attributes.pay_per_name == "month" ? CampaignBean.PledgeType.Month : CampaignBean.PledgeType.Video;
        campaign._creatorId = jCampaign.relationships.creator.data.id;

        campaign._discordServer = jCampaign.attributes.discord_server_id;


        campaign._images._campaignImageUrl = jCampaign.attributes.image_url;
        campaign._images._campaignThrumbUrl = jCampaign.attributes.image_small_url;

        campaign._mainVideo._url = jCampaign.attributes.main_video_url;
        campaign._mainVideo._youtubeEmbed = jCampaign.attributes.main_video_embed;

        campaign._thanksVideo._url = jCampaign.attributes.thanks_video_url;
        campaign._thanksVideo._youtubeEmbed = jCampaign.attributes.thanks_embed;

        //reward // goal 
        for (int i = 0; i < jIncludedInfo.Count; i++)
        {
            JsonCompaignIncludedInfo inc = jIncludedInfo[i];
            Attributes2 a = inc.attributes;
            string type = inc.type;
            if (type == "reward")
            {
                Reward r = new Reward();
                r._id = inc.id;
                r._amount = a.amount;
                r._amountCent = a.amount_cents;
                r._title = a.title;
                r._description = a.description;
                r._patreonCount = a.patron_count;
                r._isPublished = a.published;
                r._createAt = Timestamp.Create(a.created_at);
                r._editedAt = Timestamp.Create(a.edited_at);
                r._publishedAt = Timestamp.Create(a.published_at);
                r._unpublishedAt = Timestamp.Create(a.unpublished_at);
                r._deletedAt = Timestamp.Create(a.published_at);
                r._image._imageUrl = a.image_url;
                campaign._rewards.Add(r);
                campaign._rewardsId.Add(r._id);
            }
            else if (type == "goal")
            {
                Goal r = new Goal();
                r._id = inc.id;
                r._amount = (int)a.amount;
                r._amount_cent = (int)a.amount_cents;
                r._completedPourcentage = (int)a.completed_percentage;
                r._title = a.title;
                r._description = a.description;
                r._createAt = Timestamp.Create(a.created_at);
                r._reachedAt = Timestamp.Create(a.reached_at);
                r._image._imageUrl = a.image_url;
                campaign._goals.Add(r);
                campaign._goalsId.Add(r._id);

            }



        }
        return campaign;
    }
    public static void DisplayError(string message)
    {
        Debug.LogWarning(message);
    }
    #endregion

}
