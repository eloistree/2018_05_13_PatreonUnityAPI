﻿
using PatreonUnityAPI.Public.Campaign;
using PatreonUnityAPI.Public.User;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface PatreonInformationLoadable
{
    //void LoadPublic_UserInformation();
    //void LoadPublic_CampaignInformation();


    //void LoadAPI_CurrentUserInformation();
    //void LoadAPI_CurrentCampaignInformation();

}

public class PatreonJsonLoader : MonoBehaviour {

    #region SINGLETON ACCESS 
    private static PatreonJsonLoader _instanceInScene;
    public static PatreonJsonLoader Instance
    {
        get
        {   //////// Return Instance :) ///////
            bool hasInstance = _instanceInScene != null;
            if (hasInstance)
                return _instanceInScene;
            //////// Find the instance in scene /////
            _instanceInScene = FindObjectOfType<PatreonJsonLoader>();

            hasInstance = _instanceInScene != null;
            if (hasInstance)
                return _instanceInScene;

            ///////// Create the instance in the scene////////////////
            GameObject createdLoader = new GameObject("# Patreon Loader Instance");
            _instanceInScene = createdLoader.AddComponent<PatreonJsonLoader>();

            return _instanceInScene;
        }
        private set
        {
            if (_instanceInScene == null)
                _instanceInScene = value;
        }
    }

    public void Awake()
    {
        Instance = this;
    }
    #endregion
    #region SETTER AND GETTER OF THE KEYS NEEDED
    private AccessKeys _keys;
    public AccessKeys Keys
    {
        get { return _keys; }
    }
    public void SetKeysTo(AccessKeys keys) { _keys = keys; }
    public void SetKeysTo(string id, string secret, string token) { _keys = new AccessKeys(id,secret, token); }
    #endregion
    #region TOOL TO LOAD INFORMATION
    public delegate void ToDoOnLoadJsonSuccess(string jsonReceived);
    public delegate void OnErrorLoading(string error);

    public void LoadJsonFromAPI(string url, string token, ToDoOnLoadJsonSuccess toDoOnSuccess, OnErrorLoading onError)
    {
        Dictionary<string, string> header = new Dictionary<string, string>();
        header.Add("Content-Type", "application/x-www-form-urlencoded");
        header.Add("Authorization", "Bearer " + token);
        CoroutineManager.StartCoroutineAsap(LoadPage(url, null, header, toDoOnSuccess, onError));
    }
    public void LoadJsonFromWebpage(string url, ToDoOnLoadJsonSuccess toDoOnSuccess, OnErrorLoading onError)
    {
        Dictionary<string, string> header = new Dictionary<string, string>();
        header.Add("Content-Type", "application/x-www-form-urlencoded");

        CoroutineManager.StartCoroutineAsap(LoadPage(url,null, header, toDoOnSuccess, onError));
    }
    
   
    private IEnumerator LoadPage(string url, WWWForm form, Dictionary<string, string> header, ToDoOnLoadJsonSuccess toDoOnSuccess, OnErrorLoading onError)
    {
        WWW www = new WWW(url, form==null?null:form.data, header);
        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            toDoOnSuccess(www.text);
        }
        else
        {
            if (onError != null)
                onError(www.error);
        }
    }


    #endregion

    #region DATA LOADER FROM API


    public delegate void OnLoaded_Token(ref J_Token data);
    public void LoadToken(string apiUrl, string userCode, string clientId, string clientSecret, string urlRedirect, OnLoaded_Token loaded, OnErrorLoading onError)
    {
        string url = apiUrl;
        WWWForm form = new WWWForm();
        form.AddField("grant_type", "authorization_code");
        form.AddField("code", userCode);
        form.AddField("client_id", clientId);
        form.AddField("client_secret", clientSecret);
        form.AddField("redirect_uri", urlRedirect);
        Dictionary<string, string> header = new Dictionary<string, string>();
        header.Add("Content-Type", "application/x-www-form-urlencoded");
        
        ToDoOnLoadJsonSuccess result = delegate (string textToTranslate)
        {
            Debug.Log("Token Access: " + textToTranslate);
            J_Token generated = JsonUtility.FromJson<J_Token>(textToTranslate);
            loaded(ref generated);
        };
        CoroutineManager.StartCoroutineAsap(LoadPage(url, form, header, result, onError));
        
    }

    public void LoadNewToken(string apiUrl, string refreshToken, string clientId, string clientSecret, OnLoaded_Token loaded, OnErrorLoading onError)
    {
        string url = apiUrl;
        WWWForm form = new WWWForm();
        form.AddField("grant_type", "refresh_token");
        form.AddField("refresh_token", refreshToken);
        form.AddField("client_id", clientId);
        form.AddField("client_secret", clientSecret);
        Dictionary<string, string> header = new Dictionary<string, string>();
        header.Add("Content-Type", "application/x-www-form-urlencoded");

        ToDoOnLoadJsonSuccess result = delegate (string textToTranslate)
        {
            Debug.Log("Refresh Token Access: " + textToTranslate);
            J_Token generated = JsonUtility.FromJson<J_Token>(textToTranslate);
            loaded(ref generated);
        };
        CoroutineManager.StartCoroutineAsap( LoadPage(url, form, header, result, onError));
        
    }
    
    #endregion

    #region DATA LOADER FROM PUBLIC

    public delegate void OnLoaded_CampaignInfo(ref JsonCompaignInfo user, ref List<JsonCompaignIncludedInfo> included);
    public void LoadWeb_CompaignInformation(string compaignId, OnLoaded_CampaignInfo loaded, OnErrorLoading onError)
    {
        if (compaignId == null || compaignId.Length <= 0)
            throw new System.NullReferenceException();
        ToDoOnLoadJsonSuccess result = delegate (string textToTranslate)
        {
            Debug.Log("Web Campaign Info: " + textToTranslate);
            J_PublicCampaignInfo currentUser = JsonUtility.FromJson<J_PublicCampaignInfo>(textToTranslate);
            loaded(ref currentUser.data, ref currentUser.included);

        };
        string url = "https://www.patreon.com/api/campaigns/"+ compaignId;
        LoadJsonFromWebpage(url, result, onError);
    }

    public void LoadApi_CompaignInformation(string token, OnLoaded_CampaignInfo loaded, OnErrorLoading onError)
    {
        ToDoOnLoadJsonSuccess result = delegate (string textToTranslate)
        {
            Debug.Log("Web Campaign Info: " + textToTranslate);
            J_PrivateCampaignInfo currentUser = JsonUtility.FromJson<J_PrivateCampaignInfo>(textToTranslate);
            JsonCompaignInfo data = currentUser.data[0];
            loaded(ref data ,ref currentUser.included);


        };

        LoadJsonAPI(JsonToLoad.Campaign, token, result, onError);
    }

    public delegate void OnLoaded_UserInfo(ref JsonPublicData_User user);
    public void LoadWeb_UserInformation(string userId, OnLoaded_UserInfo loaded, OnErrorLoading onError)
    {
        if (userId == null || userId.Length <= 0)
            throw new System.NullReferenceException();

        ToDoOnLoadJsonSuccess result = delegate (string textToTranslate)
        {
         //   Debug.Log("Web User Info: " + textToTranslate);
            JsonRootUserInfo currentUser = JsonUtility.FromJson<JsonRootUserInfo>(textToTranslate);

            loaded(ref currentUser.data);

        };
        string url = "https://www.patreon.com/api/user/" + userId;
        LoadJsonFromWebpage(url, result, onError);

    }
    public void LoadApi_UserInformation(string token, OnLoaded_UserInfo loaded, OnErrorLoading onError)
    {
        ToDoOnLoadJsonSuccess result = delegate (string textToTranslate)
        {
            Debug.Log("Web Campaign Info: " + textToTranslate);
            JsonRootUserInfo currentUser = JsonUtility.FromJson<JsonRootUserInfo>(textToTranslate);
            loaded(ref currentUser.data);


        };

        LoadJsonAPI(JsonToLoad.User, token, result, onError);
    }


    public enum JsonToLoad { User, Campaign }
    public void LoadJsonAPI(JsonToLoad toLoad,string token, ToDoOnLoadJsonSuccess onLoaded, OnErrorLoading onError)
    {

        string url = "" ;
        switch (toLoad)
        {
            case JsonToLoad.User:
                url = "https://api.patreon.com/oauth2/api/current_user";
                break;
            case JsonToLoad.Campaign:
                url = "https://api.patreon.com/oauth2/api/current_user/campaigns?include=rewards,creator,goals,pledges";
                break;
            default:
                return;
        }

        LoadJsonFromAPI(url,token, onLoaded, onError);

    }

    public void LoadJsonAPI(string campaignId, string token,  ToDoOnLoadJsonSuccess onLoaded, OnErrorLoading onError)
    {
        string url = string.Format("https://api.patreon.com/oauth2/api/campaigns/{0}/pledges", campaignId);
        LoadJsonFromAPI(url, token, onLoaded, onError);

    }
    #endregion


}
