﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PatreonList : MonoBehaviour {


    public PatreonIdentifiers _patreons = new PatreonIdentifiers();

    [Header("Patreon Id (Finish by /)")]
    public string _patreonIdInput;

    [Header("Char that separate ids")]
    public char[] _idSeparator = new char[] { ' ', ':', ',', ';' };

    public List<string> GetPatreon(PatreonIdentifiers.SortBy sortBy)
    {
        return _patreons.GetPatreon(sortBy);
    }
    void Awake() {
        StringIdsToList();
    }
	void OnValidate () {
        StringIdsToList();
    }
    void StringIdsToList() {
        if (_patreonIdInput.Length <2 )
            return;
        if (!(_patreonIdInput[_patreonIdInput.Length-1] == '/'))
            return;
        _patreonIdInput = _patreonIdInput.Substring(0, _patreonIdInput.Length - 1);

        string[] ids = _patreonIdInput.Split(_idSeparator);


        for (int i = 0; i < ids.Length; i++)
        {
            _patreons.AddId(ids[i]);
        }
        _patreonIdInput = "";
    }
}

[System.Serializable]
public class  PatreonIdentifiers
{
    [SerializeField]
    private List<string> _ids = new List<string>();
    public void AddId(string id) { _ids.Add(id); }


    public enum SortBy { Ascending, Descending, Random }
    public List<string> GetPatreon(SortBy sortBy) {
        List<string> hey = new List<string>(_ids);
        switch (sortBy)
        {
            case SortBy.Descending:
                return  hey.OrderBy(x => x.ToString()).ToList();
            case SortBy.Random:
                return  hey.OrderBy(x => Random.value).ToList();
            default:
                return hey.OrderByDescending(x => x.ToString()).ToList();
        }
    }

    public void Clear() {
        _ids.Clear();
    }

}
