﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
public class ScreenCapture : MonoBehaviour
{
    public string _path = "";
    public Camera _linkedCamera;

    public string _tagToDeactivate="HideForCapture";
    public GameObject[] _targetToDeactivate;

    private RenderTexture _overviewTexture;

    void LateUpdate()
    {

        if (Input.GetKeyDown("f9"))
        {
            LaunchScreenShot();
        }

    }

    public void LaunchScreenShot() {
        StartCoroutine(TakeScreenShot());

    }

    // return file name
    string fileName(int width, int height)
    {
        return string.Format("screen_{0}x{1}_{2}.png",
                              width, height,
                              System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
    }

    public IEnumerator TakeScreenShot()
    {

        _targetToDeactivate = GameObject.FindGameObjectsWithTag(_tagToDeactivate);
        for (int i = 0; i < _targetToDeactivate.Length; i++)
        {
            _targetToDeactivate[i].SetActive(false);
        }
        yield return new WaitForEndOfFrame();

        Camera camOV = _linkedCamera == null ? Camera.main : _linkedCamera; ;

        RenderTexture currentRT = RenderTexture.active;

        RenderTexture.active = camOV.targetTexture;
        camOV.Render();
        Texture2D imageOverview = new Texture2D(camOV.targetTexture.width, camOV.targetTexture.height, TextureFormat.RGB24, false);
        imageOverview.ReadPixels(new Rect(0, 0, camOV.targetTexture.width, camOV.targetTexture.height), 0, 0);
        imageOverview.Apply();
        RenderTexture.active = currentRT;


        // Encode texture into PNG
        byte[] bytes = imageOverview.EncodeToPNG();

        // save in memory
        string filename = fileName(Convert.ToInt32(imageOverview.width), Convert.ToInt32(imageOverview.height));

        string directory = Application.dataPath + "/../Snapshots/";
        Directory.CreateDirectory(directory);
        _path = directory + filename;
        Application.CaptureScreenshot(_path);

        System.IO.File.WriteAllBytes(_path, bytes);
        yield return new WaitForEndOfFrame();
        for (int i = 0; i < _targetToDeactivate.Length; i++)
        {
            _targetToDeactivate[i].SetActive(true);
        }
    }
}