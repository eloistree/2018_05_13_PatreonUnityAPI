﻿
using System;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public class SocialUtility
{

    public enum UrlType { Image, Accout }
    public enum SizeType { Small, Normal, Large }

    [Serializable]
    public abstract class SocialInterface
    {
       
        public abstract string GetUserIdFrom(string url);
        public abstract string GetAccountUrlFromId(string id);
        public abstract string GetProfileUrlFromId(string idName, SizeType size);
        public abstract void IsExisting(string id, Action<bool> isIdExisting);

        public bool HasUser(string url)
        {

            return !string.IsNullOrEmpty(GetUserIdFrom(url));
        }
        public string LoadProfileUrlFromUrl(string url, SizeType size)
        {
            return GetProfileUrlFromId(GetUserIdFrom(url), size);
        }

       
    }

    public enum SocialInfoType
    {
        Gravatar, Twitter, Twitch, Facebook, Youtube, Discord, Spotify, DeviantArt, Patreon
            //Missing Instagram , 
    }
    public static  SocialInterface GetSocialInterface(SocialInfoType type ) {
        switch (type)
        {
            case SocialInfoType.Gravatar:return Gravatar;
            case SocialInfoType.Twitter: return Gravatar;
            case SocialInfoType.Twitch: return Twitch;
            case SocialInfoType.Youtube: return Youtube;
            case SocialInfoType.Discord: return Discord;
            case SocialInfoType.Spotify: return Spotify;
            case SocialInfoType.DeviantArt: return Deviantart;
            case SocialInfoType.Patreon: return Patreon;
            default:
                throw new Exception("Should not be here !");
        }
    }

    public static GravatarAccess Gravatar = new GravatarAccess();
    public static TwitterAccess Twitter = new TwitterAccess();
    public static TwitchAccess Twitch = new TwitchAccess();
    public static FacebookAccess Facebook = new FacebookAccess();
    public static YoutubeAccess Youtube = new YoutubeAccess();
    public static DiscordAccess Discord = new DiscordAccess();
    public static SpotifyAccess Spotify = new SpotifyAccess();
    public static DeviantartAccess Deviantart = new DeviantartAccess();
    public static PatreonAccess Patreon = new PatreonAccess();

    #region Done (Social Interface Connection)
    public class GravatarAccess : SocialInterface
    {
        public override string GetUserIdFrom(string url)
        {
            //http://gravatar.com/avatar/
            string pattern = @"http[s]?:[//]*[w]*[.]*gravatar.com\/avatar\/";
            url = Regex.Replace(url, pattern, "");
            return url;

        }

        public override string GetProfileUrlFromId(string idName, SizeType size)
        {
            string id = idName;
            if (idName.IndexOf('@') > -1)
                id = HashEmailForGravatar(idName);

            int imageSize = 64;
            switch (size)
            {
                case SizeType.Small:
                    imageSize = 64;
                    break;
                case SizeType.Normal:
                    imageSize = 256;
                    break;
                case SizeType.Large:
                    imageSize = 512;
                    break;
                default:
                    break;
            }
            return "http://gravatar.com/avatar/" + id + "?s=" + imageSize + "&d=mm";
        }

        public static string HashEmailForGravatar(string email)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(email));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        public override void IsExisting(string id, Action<bool> isIdExisting)
        {
            CoroutineManager.StartCoroutineAsap(CheckIfIdExist(id, isIdExisting));
        }
        public IEnumerator CheckIfIdExist(string id, Action<bool> isIdExisting)
        {
            yield return new WaitForSeconds(1);
            //TODO LATER
            isIdExisting.Invoke(true);
        }
        
        public override string GetAccountUrlFromId(string id)
        {
                return "http://gravatar.com/" + id;
        }
    }

    public class TwitterAccess : SocialInterface
    {
        public enum Size { Mini, Normal, Bigger, Original }
        public override string GetUserIdFrom(string url)
        {

            string pattern = @"http://gravatar.com/avatar/\/";
            url = Regex.Replace(url, pattern, "");
            return url;
        }
        public override string GetProfileUrlFromId(string idName, SizeType size)
        {
            Size sizeToCall = Size.Normal;
            switch (size)
            {
                case SizeType.Small:
                    sizeToCall = Size.Mini;
                    break;
                case SizeType.Normal:
                    sizeToCall = Size.Original;
                    break;
                case SizeType.Large:
                    sizeToCall = Size.Bigger;
                    break;
                default:
                    break;
            }


            if (string.IsNullOrEmpty(idName))
                return "";
            return "https://twitter.com/" + idName + "/profile_image?size=" + sizeToCall.ToString().ToLower();

        }
        public override void IsExisting(string id, Action<bool> isIdExisting)
        {
            CoroutineManager.StartCoroutineAsap(CheckIfIdExist(id, isIdExisting));
        }
        public IEnumerator CheckIfIdExist(string id, Action<bool> isIdExisting)
        {
            yield return new WaitForSeconds(1);
            //TODO LATER
            isIdExisting.Invoke(true);
        }
        
        public override string GetAccountUrlFromId(string id)
        {
           
                return "https://twitter.com/" + id;
        }
    }

    public class FacebookAccess : SocialInterface
    {
        public enum Size { Small, Normal, Square, Large }
        public override string GetUserIdFrom(string url)
        {

            string pattern = @"http[s]?:[//]*[w]*[.]*facebook.com\/";
            url = Regex.Replace(url, pattern, "");
            return url;
        }
        public override string GetProfileUrlFromId(string idName, SocialUtility.SizeType size)
        {
            Size sizeToCall = Size.Normal;
            switch (size)
            {
                case SizeType.Small:
                    sizeToCall = Size.Small;
                    break;
                case SizeType.Normal:
                    sizeToCall = Size.Normal;
                    break;
                case SizeType.Large:
                    sizeToCall = Size.Large;
                    break;
                default:
                    break;
            }


            if (string.IsNullOrEmpty(idName))
                return "";
            return "https://graph.facebook.com/" + idName + "/picture?type=" + sizeToCall.ToString().ToLower();

        }
        public override void IsExisting(string id, Action<bool> isIdExisting)
        {
            CoroutineManager.StartCoroutineAsap(CheckIfIdExist(id, isIdExisting));
        }
        public IEnumerator CheckIfIdExist(string id, Action<bool> isIdExisting)
        {
            yield return new WaitForSeconds(1);
            //TODO LATER
            isIdExisting.Invoke(true);
        }

        public override string GetAccountUrlFromId(string id)
        {
            return "https://facebook.com/"+id;
        }
    }
    #endregion

    #region To Do (Social Interface Connection)

    public class TwitchAccess : SocialInterface
    {
        public override string GetUserIdFrom(string url)
        {
            throw new NotImplementedException();
        }

        public override string GetProfileUrlFromId(string idName, SizeType size)
        {
            throw new NotImplementedException();
        }
        public override void IsExisting(string id, Action<bool> isIdExisting)
        {
            throw new NotImplementedException();
        }

        public override string GetAccountUrlFromId(string id)
        {
            throw new NotImplementedException();
        }
    }
    public class YoutubeAccess : SocialInterface
    {
        public override string GetUserIdFrom(string url)
        {
            throw new NotImplementedException();
        }

        public override string GetProfileUrlFromId(string idName, SizeType size)
        {
            throw new NotImplementedException();
        }
        public override void IsExisting(string id, Action<bool> isIdExisting)
        {
            throw new NotImplementedException();
        }

        public override string GetAccountUrlFromId(string id)
        {
            throw new NotImplementedException();
        }
    }
    public class DiscordAccess : SocialInterface
    {
        public override string GetUserIdFrom(string url)
        {
            throw new NotImplementedException();
        }

        public override string GetProfileUrlFromId(string idName, SizeType size)
        {
            throw new NotImplementedException();
        }
        public override void IsExisting(string id, Action<bool> isIdExisting)
        {
            throw new NotImplementedException();
        }

        public override string GetAccountUrlFromId(string id)
        {
            throw new NotImplementedException();
        }
    }
    public class DeviantartAccess : SocialInterface
    {
        public override string GetUserIdFrom(string url)
        {

            string subString = url;
            string patternAvatar = @"http[s]?:[//]*a.deviantart.(net|com)/avatars/[A-z0-9]*/[A-z0-9]*/";
            string patternWebsite = @".deviantart.(net|com)/";
            string patternHttp = @"http[s]?:[//]*";
            string patternImage = @".(jpg|jpeg|png)";
            if (Regex.IsMatch(url, patternAvatar))
            {
                subString = Regex.Replace(subString, patternAvatar, "");
                subString = Regex.Replace(subString, patternImage, "");
            }
            else if (Regex.IsMatch(url, patternWebsite))
            {
                subString = Regex.Replace(subString, patternHttp, "");
                subString = Regex.Replace(subString, patternWebsite, "");
            }
            return subString;
        }

        public override string GetProfileUrlFromId(string idName, SizeType size)
        {

            throw new NotImplementedException();
            //return "http://a.deviantart.net/avatars/c/e/" + idName + ".jpg";
        }
        public override void IsExisting(string id, Action<bool> isIdExisting)
        {
            throw new NotImplementedException();
        }

        public override string GetAccountUrlFromId(string id)
        {
            return "http://" + id + ".deviantart.com/";
        }
    }
    public class SpotifyAccess : SocialInterface
    {
        public override string GetAccountUrlFromId(string id)
        {
            throw new NotImplementedException();
        }

        public override string GetProfileUrlFromId(string idName, SizeType size)
        {
            throw new NotImplementedException();
        }

        public override string GetUserIdFrom(string url)
        {
            throw new NotImplementedException();
        }

        public override void IsExisting(string id, Action<bool> isIdExisting)
        {
            throw new NotImplementedException();
        }
    }
    public class PatreonAccess : SocialInterface
    {
        public override string GetUserIdFrom(string url)
        {
            //https://www.patreon.com/api/user/|ID|
            throw new NotImplementedException();
        }

        public override string GetProfileUrlFromId(string idName, SizeType size)
        {
            throw new NotImplementedException();
        }
        public override void IsExisting(string id, Action<bool> isIdExisting)
        {
            throw new NotImplementedException();
        }

        public override string GetAccountUrlFromId(string id)
        {
            return "https://www.patreon.com/user/"+id;
        }

        public static  bool IsDefaultImage(string url)
        {
            bool isDefaultPhoto;
            string pattern = @"http[s]?:[//]*[A-z0-9]*.patreon.com\/[0-9]*\/[0-9]*\/[0-9]*";
            isDefaultPhoto = Regex.IsMatch(url, pattern);
            return isDefaultPhoto;
        }
    
    }
    #endregion

    [System.Serializable]
    public struct SocialInfo
    {
        [SerializeField]
        public SocialInterface _linkedSocialInterface;

        public SocialInfo(SocialInterface socialInterface)
        {
            _linkedSocialInterface = socialInterface;
            _accountUrl = "";
            _userId = "";
        }

        public void SetGivenData(string accountUrl, string userId)
        {
            if (string.IsNullOrEmpty(accountUrl) && !string.IsNullOrEmpty(userId))
            {
                accountUrl = _linkedSocialInterface.GetAccountUrlFromId(userId);
            }
            if (!string.IsNullOrEmpty(accountUrl) && string.IsNullOrEmpty(userId))
            {
                userId = _linkedSocialInterface.GetUserIdFrom(accountUrl);
            }

            _accountUrl = accountUrl;
            _userId = userId;
        }
        [SerializeField]
        private string _accountUrl;

        [SerializeField]
        private string _userId;

        public string GetImageUrl(SizeType size = SizeType.Large)
        {
            if (!string.IsNullOrEmpty(_userId))
                return _linkedSocialInterface
                    .GetProfileUrlFromId(
                    _userId, size);
            return "";
        }
        public string GetAccountUrl() { return _accountUrl; }
        public string GetUserId() { return _userId; }
        
    }
}
